FROM registry.gitlab.com/enarx/lab:latest

# Install and configure the kernel
RUN apt update \
 && apt install --no-install-recommends -y initramfs-tools \
 && ln -sf /bin/true /usr/sbin/update-initramfs \
 && apt install --no-install-recommends -y linux-image-generic \
 && rm -rf /var/lib/apt/lists/*
RUN cd /boot; ln -s vmlinuz* wyrcan.kernel

# Install the AESM and PCCS daemon services
COPY pccs.service /etc/systemd/system/pccs.service
RUN systemctl enable pccs.service

# Install the AESM daemon service and various Intel utils
ADD sgx_debian_local_repo.tgz /opt/intel
RUN echo 'deb [trusted=yes] file:///opt/intel/sgx_debian_local_repo buster main' > /etc/apt/sources.list.d/sgx_debian_local_repo.list \
 && echo 'deb http://deb.debian.org/debian buster main contrib non-free' > /etc/apt/sources.list.d/buster.list \
 && apt update \
 && apt install -y libsgx-epid libsgx-quote-ex libsgx-dcap-ql sgx-aesm-service libsgx-dcap-default-qpl libsgx-dcap-quote-verify sgx-pck-id-retrieval-tool sgx-ra-service \
 && rm -rf /var/lib/apt/lists/* \
 && userdel aesmd
# We install the aesmd user via our own service

COPY sgx_sysusers.conf /lib/sysusers.d/sgx.conf
COPY sgx_default_qcnl.conf /etc/sgx_default_qcnl.conf
